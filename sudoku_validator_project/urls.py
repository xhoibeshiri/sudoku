from django.urls import path, include

urlpatterns = [
    path('sudoku/', include('sudoku_validator.urls')),
]
