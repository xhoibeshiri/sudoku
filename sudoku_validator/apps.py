from django.apps import AppConfig


class SudokuValidatorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sudoku_validator'
