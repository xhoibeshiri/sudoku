import json

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from .utils import is_valid_sudoku


@csrf_exempt
def validate_sudoku(request):
    if request.method == 'POST':
        try:
            # Parse JSON data
            data = json.loads(request.body.decode('utf-8'))

            # Extract board data
            board = data.get('board', [])

            # Validate Sudoku
            is_valid = is_valid_sudoku(board)

            return JsonResponse({'valid': is_valid})
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=400)

    return JsonResponse({'error': 'Invalid request method'})
