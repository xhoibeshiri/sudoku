from django.urls import path
from .views import validate_sudoku

urlpatterns = [
    path('validate/', validate_sudoku, name='validate_sudoku'),
]
