from math import isqrt


def is_valid_sudoku(board):
    n = len(board)
    root_n = isqrt(n)

    # Validate rows
    if any(not is_valid_set(row, n) for row in board):
        return False

    # Validate columns
    for col in range(n):
        column = [board[row][col] for row in range(n)]
        if not is_valid_set(column, n):
            return False

    # Validate little squares
    for row in range(0, n, root_n):
        for col in range(0, n, root_n):
            square = [board[i][j] for i in range(row, row + root_n) for j in range(col, col + root_n)]
            if not is_valid_set(square, n):
                return False

    return True


def is_valid_set(nums, n):
    return sorted(nums) == list(range(1, n + 1))
